import 'package:flutter/material.dart';

const Color bluishClr = Color(0XFF4E5AE8);
const Color orangeClr = Color(0XFFDC4227);
const Color yellowClr = Color(0XFFF39E26);
const Color whiteClr = Colors.white;
const Color darkGreyClr = Color(0XFF121212);
const Color darkHeaderClr = Color(0XFF424242);
const primaryClr = bluishClr;

class Themes {
  static final light = ThemeData(
    backgroundColor: Colors.white,
    primaryColor: primaryClr,
    brightness: Brightness.light,
  );
  static final dark = ThemeData(
    backgroundColor: darkGreyClr,
    primaryColor: darkGreyClr,
    brightness: Brightness.dark,
  );
}