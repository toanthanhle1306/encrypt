part of 'app_pages.dart';

abstract class Routes {
  static const HOME = '/home';
  static const CEASAR = '/ceasar';
  static const PERMUTATION = '/permutation';
  static const AFFINE = '/affine';
  static const VIGENERE = '/vigenere';
  static const HILL = '/hill';
  static const RSA = '/rsa';
  static const ELGAMAL = '/elgamal';
}
