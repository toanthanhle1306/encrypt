import 'package:encrypt/modules/affine/affine_binding.dart';
import 'package:encrypt/modules/affine/affine_page.dart';
import 'package:encrypt/modules/elgamal/elgamal_binding.dart';
import 'package:encrypt/modules/elgamal/elgamal_page.dart';
import 'package:encrypt/modules/hill/hill_binding.dart';
import 'package:encrypt/modules/hill/hill_page.dart';
import 'package:encrypt/modules/rsa/rsa_binding.dart';
import 'package:encrypt/modules/rsa/rsa_page.dart';
import 'package:encrypt/modules/vigenere/vigenere_binding.dart';
import 'package:encrypt/modules/vigenere/vigenere_page.dart';
import 'package:get/get.dart';
import '../modules/ceasar/ceasar_binding.dart';
import '../modules/ceasar/ceasar_page.dart';
import '../modules/home/home_binding.dart';
import '../modules/home/home_page.dart';
import '../modules/permutation/permutation_binding.dart';
import '../modules/permutation/permutation_page.dart';
part 'app_routes.dart';

abstract class AppPages {
  static final pages = [
    GetPage(
      name: Routes.HOME,
      page: () => const HomePage(),
      binding: HomeBinding(),
    ),
    GetPage(
      name: Routes.CEASAR,
      page: () => const CeasarPage(),
      binding: CeasarBinding(),
    ),
    GetPage(
      name: Routes.PERMUTATION,
      page: () => const PermutationPage(),
      binding: PermutationBinding(),
    ),
    GetPage(
      name: Routes.AFFINE,
      page: () => const AffinePage(),
      binding: AffineBinding(),
    ),
    GetPage(
      name: Routes.VIGENERE,
      page: () => const VigenerePage(),
      binding: VigenereBinding(),
    ),
    GetPage(
      name: Routes.HILL,
      page: () => const HillPage(),
      binding: HillBinding(),
    ),
    GetPage(
      name: Routes.RSA,
      page: () => const RSAPage(),
      binding: RSABinding(),
    ),
    GetPage(
      name: Routes.ELGAMAL,
      page: () => const ElgamalPage(),
      binding: ElgamalBinding(),
    ),
  ];
}
