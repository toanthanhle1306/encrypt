import 'package:encrypt/routes/app_pages.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'common/theme/themes.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  var getMaterialApp = GetMaterialApp(
    builder: EasyLoading.init(),
    debugShowCheckedModeBanner: false,
    theme: Themes.light,
    darkTheme: Themes.dark,
    getPages: AppPages.pages,
    initialRoute: Routes.HOME,
    initialBinding: BindingsBuilder(() {}),
  );

  runApp(getMaterialApp);
}
