// ignore_for_file: constant_identifier_names

class ClassicalCipher {
  final String title;
  final ClassicalCipherType type;

  ClassicalCipher(this.title, this.type);

  static List<ClassicalCipher> cipherList = <ClassicalCipher>[
    ClassicalCipher('Mã hóa Ceasar', ClassicalCipherType.Ceasar),
    ClassicalCipher('Mã hóa Permutation', ClassicalCipherType.Permutation),
    ClassicalCipher('Mã hóa Affine', ClassicalCipherType.Affine),
    ClassicalCipher('Mã hóa Vigenere', ClassicalCipherType.Vigenere),
    ClassicalCipher('Mã hóa Hill', ClassicalCipherType.Hill),
    ClassicalCipher('Mã hóa RSA', ClassicalCipherType.RSA),
    ClassicalCipher('Mã hóa Elgamal', ClassicalCipherType.Elgamal),
  ];
}

enum ClassicalCipherType { Ceasar, Permutation, Affine, Vigenere, Hill, RSA, Elgamal }
