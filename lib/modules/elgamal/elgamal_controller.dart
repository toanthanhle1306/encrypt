import 'dart:convert';

import 'package:encrypt/modules/elgamal/components/key_elgamal_generator.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';

class ElgamalController extends GetxController
    with GetSingleTickerProviderStateMixin {
  late TabController tabController;

  final inputTextController = TextEditingController();
  final keyController = TextEditingController();
  final outputTextController = TextEditingController();
  final Rx<KeyElgamal> keyElgamalObject = KeyElgamal().obs;

  @override
  void onInit() async {
    super.onInit();
    tabController = TabController(vsync: this, length: 2);
    tabController.addListener(() {
      if (tabController.indexIsChanging) {
        keyController.clear();
        outputTextController.clear();
      }
    });
  }

  void processElgamal(
      {required String text, required String key, required bool isEncrypt}) {
    String _text = text;
    List<String> ketQuaMaHoa = [];
    List<int> ketQuaGiaiMa = [];

    // Xem class key_generator trong thư mục components để xem phiN
    if (isEncrypt) {
      try {
        // print(keyElgamalObject.value.toString());
        // Kiểm tra key đã tạo chưa
        if (keyElgamalObject.value.publicKey.isEmpty ||
            keyElgamalObject.value.privateKey.isEmpty) {
          EasyLoading.showToast('Bạn phải tạo khóa trước!');
          return;
        }

        // Lấy dữ liệu từ key đã tạo
        var publicKeyPAB = keyElgamalObject.value.publicKey.split(",");
        int p = int.parse(publicKeyPAB.elementAt(0));
        int alpha = int.parse(publicKeyPAB.elementAt(1));
        int beta = int.parse(publicKeyPAB.elementAt(2));

        // String privateKey = keyElgamalObject.value.privateKey;
        // int a = int.parse(privateKey);
        // print("p=$p,  alpha=$alpha,  beta=$beta,  a=$a");

        int k = int.parse(key);
        if (k >= p) {
          return;
        }

        var plainText = utf8.encode(text);
        print(plainText);

        for (int charNum in plainText) {
          BigInt y1 = BigInt.from(alpha.modPow(k, p));
          // (x*beta^k) mod b
          int temp = charNum.modPow(1, p) * beta.modPow(k, p);
          BigInt y2 = BigInt.from(temp.modPow(1, p));
          
          if (y2.isValidInt) {
            int newY2 = y2.toInt() % p;

            ketQuaMaHoa.add("($y1 - $newY2)");
          } else {
            ketQuaMaHoa.add("($y1 - $y2)");
          }
        }
        // print("ketQuaMaHoa" + ketQuaMaHoa.toString());
      } catch (e) {
        EasyLoading.showError('Key phải là 1 số nhỏ hơn p!');
      }

      outputTextController.text = ketQuaMaHoa.toString();
    } else {
      var cipherText = _text
          .replaceAll(RegExp(r'\['), "")
          .replaceAll(RegExp(r'\]'), "")
          .removeAllWhitespace
          .split(",");

      List<String> listMaHoa = [];
      for (String giaTri in cipherText) {
        listMaHoa.add(giaTri);
      }
      // print(listMaHoa);

      try {
        var publicKeyPAB = keyElgamalObject.value.publicKey.split(",");
        int p = int.parse(publicKeyPAB.elementAt(0));

        // Lấy khóa bí mật a từ controller
        // String privateKey = keyElgamalObject.value.privateKey;
        // int a = int.parse(privateKey);

        // Lấy khóa bí mật a từ người dùng nhập
        int keyAFromUser = int.parse(key);
        for (var cipher in listMaHoa) {
          var pairNumber = cipher
              .replaceAll(RegExp(r'\('), "")
              .replaceAll(RegExp(r'\)'), "")
              .split("-");
          int y1 = int.parse(pairNumber[0]);
          int y2 = int.parse(pairNumber[1]);

          int t1 = y1.modPow(keyAFromUser, p);
          int t2 = t1.modInverse(p);
          int x = (y2 * t2) % p;
          ketQuaGiaiMa.add(x);
        }
        // print("ketQuaGiaiMa" + ketQuaGiaiMa.toString());

        var plainTextGiaiMa = utf8.decode(ketQuaGiaiMa); // từ số -> text
        // print(plainTextGiaiMa);
        outputTextController.text = plainTextGiaiMa.toString();
      } catch (e) {
        EasyLoading.showToast('Key phải là 1 số (a)!');
      }
    }
  }
}
