import 'package:get/get.dart';
import 'package:get/instance_manager.dart';
import 'elgamal_controller.dart';

class ElgamalBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => ElgamalController());
  }
}
