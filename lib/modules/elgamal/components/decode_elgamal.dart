import 'package:encrypt/modules/elgamal/elgamal_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../widgets/button_widget.dart';
import '../../widgets/input_widget.dart';

class DecodeElgamal extends StatefulWidget {
  final TextEditingController plainTextController;
  final TextEditingController keyController;
  final TextEditingController cipherTextController;
  final Function() onTap;

  const DecodeElgamal(
      {Key? key,
      required this.plainTextController,
      required this.keyController,
      required this.cipherTextController,
      required this.onTap})
      : super(key: key);

  @override
  State<DecodeElgamal> createState() => _DecodeElgamalState();
}

class _DecodeElgamalState extends State<DecodeElgamal> {
  late TextEditingController _cipherTextController;
  @override
  void initState() {
    _cipherTextController = widget.cipherTextController;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Flexible(
          flex: 1,
          child: InputWidget(
            hintText: 'Nhập bản mã của bạn vào đây',
            title: 'Bản mã',
            controller: widget.plainTextController,
            validator: (value) {
              return value?.isNotEmpty == true ? null : 'Vui lòng nhập bản mã';
            },
          ),
        ),
        Flexible(
          flex: 1,
          child: Column(
            children: [
              GetBuilder<ElgamalController>(builder: ((controller) {
                return Container(
                  height: 80,
                  margin: const EdgeInsets.only(top: 50.0),
                  child: ListView(
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(6.0),
                        child: SelectableText(
                          "Khoá công khai: (p, alpha, beta):\n " +
                              controller.keyElgamalObject.value.publicKey,
                          style: const TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(6.0),
                        child: SelectableText(
                          "Khoá bí mật (a): " + controller.keyElgamalObject.value.privateKey,
                          style: const TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ],
                    scrollDirection: Axis.vertical,
                  ),
                );
              })),
              SizedBox(
                height: 300,
                child: InputWidget(
                  // keyboardType: TextInputType.number,
                  margin: const EdgeInsets.symmetric(vertical: 50),
                  padding:
                      const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
                  hintText: 'Nhập khóa a của bạn vào đây.',
                  title: 'Khóa bí mật',
                  maxLines: 2,
                  controller: widget.keyController,
                  validator: (value) {
                    if (value?.isNotEmpty == true) {
                      if (RegExp(r'^[0-9]+$').hasMatch(value!)) {
                        return null;
                      } else {
                        return 'Key phải là số (a).';
                      }
                    } else {
                      return 'Vui lòng nhập khóa';
                    }
                  },
                ),
              ),
              ButtonWidget(
                title: 'Bắt đầu giải mã',
                onTap: widget.onTap,
              ),
            ],
          ),
        ),
        Flexible(
          flex: 1,
          child: InputWidget(
            hintText: 'Bản rõ sẽ xuất hiện ở đây',
            // enable: false,
            readOnly: true,
            title: 'Bản rõ',
            controller: _cipherTextController,
          ),
        ),
      ],
    );
  }
}
