import 'package:encrypt/modules/elgamal/components/key_elgamal_generator.dart';
import 'package:encrypt/modules/elgamal/elgamal_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../widgets/button_widget.dart';
import '../../widgets/input_widget.dart';

class EncodeElgamal extends StatefulWidget {
  final TextEditingController plainTextController;
  final TextEditingController keyController;
  final TextEditingController cipherTextController;
  final Function encodeButton;
  // final Function generateKey;

  const EncodeElgamal({
    Key? key,
    required this.plainTextController,
    required this.keyController,
    required this.cipherTextController,
    required this.encodeButton,
    // required this.generateKey,
  }) : super(key: key);

  @override
  State<EncodeElgamal> createState() => _EncodeElgamalState();
}

class _EncodeElgamalState extends State<EncodeElgamal> {
  late TextEditingController _cipherTextController;
  final _formKey = GlobalKey<FormState>();
  KeyElgamal keyElgamal = KeyElgamal();
  @override
  void initState() {
    _cipherTextController = widget.cipherTextController;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Flexible(
            flex: 1,
            child: InputWidget(
              hintText: 'Nhập bản rõ của bạn vào đây',
              title: 'Bản rõ',
              controller: widget.plainTextController,
              validator: (value) {
                return value?.isNotEmpty == true
                    ? null
                    : 'Vui lòng nhập bản rõ';
              },
            ),
          ),
          Flexible(
            flex: 1,
            child: Column(
              children: [
                GetBuilder<ElgamalController>(builder: (controller) {
                  return Obx(() {
                    return Container(
                      height: 80,
                      margin: const EdgeInsets.only(top: 50.0),
                      child: ListView(
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(6.0),
                            child: SelectableText(
                              "Khoá công khai (p, alpha, beta):\n " +
                                  controller.keyElgamalObject.value.publicKey,
                              style: const TextStyle(
                                fontSize: 16,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(6.0),
                            child: SelectableText(
                              "Khoá bí mật (a): " +
                                  controller.keyElgamalObject.value.privateKey,
                              style: const TextStyle(
                                fontSize: 16,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                        ],
                        scrollDirection: Axis.vertical,
                      ),
                    );
                  });
                }),
                GetBuilder<ElgamalController>(builder: (controller) {
                  return Center(
                    child: TextButton(
                      child: const Text(
                        'Tạo khóa',
                      ),
                      style: TextButton.styleFrom(
                        backgroundColor: Colors.yellow,
                        textStyle: const TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.bold,
                        ),
                        padding: const EdgeInsets.all(16.0),
                      ),
                      onPressed: () {
                        controller.keyElgamalObject.value.generateKeys();
                        controller.refresh();
                        // print(controller.keyElgamalObject.value.publicKey);
                        // print(controller.keyElgamalObject.value.privateKey);
                      },
                    ),
                  );
                }),
                SizedBox(
                  height: 300,
                  child: InputWidget(
                    // keyboardType: TextInputType.number,
                    margin: const EdgeInsets.symmetric(vertical: 50),
                    padding: const EdgeInsets.symmetric(
                        horizontal: 20, vertical: 20),
                    hintText: 'Nhập 1 số k bất kì nhỏ hơn p',
                    title: 'Khóa công khai',
                    maxLines: 2,
                    controller: widget.keyController,
                    validator: (value) {
                      if (value?.isNotEmpty == true) {
                        if (value!.contains(RegExp(r'^[0-9]+$'))) {
                          return null;
                        } else {
                          return 'Key phải là 1 số.';
                        }
                      } else {
                        return 'Vui lòng nhập khóa';
                      }
                    },
                  ),
                ),
                ButtonWidget(
                  title: 'Bắt đầu mã hóa',
                  onTap: () {
                    if (_formKey.currentState!.validate()) {
                      widget.encodeButton();
                    }
                  },
                ),
              ],
            ),
          ),
          Flexible(
            flex: 1,
            child: InputWidget(
              hintText: 'Bản mã sẽ xuất hiện ở đây',
              // enable: false,
              readOnly: true,
              title: 'Bản mã',
              controller: _cipherTextController,
            ),
          ),
        ],
      ),
    );
  }
}
