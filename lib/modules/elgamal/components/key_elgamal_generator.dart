import 'dart:math';

class KeyElgamal {
  late String _publicKey;
  late String _privateKey;
  String get publicKey => _publicKey;
  String get privateKey => _privateKey;

  KeyElgamal() {
    _publicKey = "";
    _privateKey = "";
  }

  int _randomOddNumberRange(min, max) {
    int temp = min + Random.secure().nextInt(max - min);
    if (temp % 2 == 0) {
      temp -= 1;
    }
    if (temp % 10 == 0) {
      temp += 1;
    }
    if (temp % 5 == 0) {
      temp += 2;
    }
    return temp;
  }

  bool _checkIfPrime(int number) {
    if (number % 2 == 0) return false;
    var limit = sqrt(number).toInt();
    if (number % limit == 0) return false;

    for (var i = 3; i <= limit; i = i + 2) {
      if (number % i == 0) {
        return false;
      }
    }
    return true;
  }

  void generateKeys() {
    bool valid = false;
    int p = _randomOddNumberRange(80000, 99999);
    int alpha = _randomOddNumberRange(25000, 50000);
    int a = _randomOddNumberRange(10000, 25000);

    while (!valid) {
      if (_checkIfPrime(p) && _checkIfPrime(alpha) && _checkIfPrime(a)) {
        valid = true;
        break;
      }
      p = _randomOddNumberRange(80000, 99999);
      alpha = _randomOddNumberRange(25000, 50000);
      a = _randomOddNumberRange(10000, 25000);
    }
    
    // In giá trị p, alpha, a và beta
    int beta = alpha.modPow(a, p);
    print("p=$p,  alpha=$alpha,  beta=$beta,  a=$a");

    // print("e=$e,  d=$d");
    _publicKey = "$p, $alpha, $beta";
    _privateKey = "$a";
  }

  @override
  String toString() {
    return "{$publicKey,  $privateKey}";
  }
}
