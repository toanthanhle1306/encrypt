import 'package:encrypt/modules/utils/utils.dart';
import 'package:flutter/material.dart';

import '../../widgets/button_widget.dart';
import '../../widgets/input_widget.dart';

class EncodePermutation extends StatefulWidget {
  final TextEditingController plainTextController;
  final TextEditingController keyController;
  final TextEditingController cipherTextController;
  final Function onTap;

  const EncodePermutation(
      {Key? key,
      required this.plainTextController,
      required this.keyController,
      required this.cipherTextController,
      required this.onTap})
      : super(key: key);

  @override
  State<EncodePermutation> createState() => _EncodePermutationState();
}

class _EncodePermutationState extends State<EncodePermutation> {
  late TextEditingController _cipherTextController;
  final _formKey = GlobalKey<FormState>();
  @override
  void initState() {
    _cipherTextController = widget.cipherTextController;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Flexible(
            flex: 1,
            child: InputWidget(
              hintText: 'Nhập bản rõ của bạn vào đây',
              title: 'Bản rõ',
              controller: widget.plainTextController,
              validator: (value) {
                return value?.isNotEmpty == true
                    ? null
                    : 'Vui lòng nhập bản rõ';
              },
            ),
          ),
          Flexible(
            flex: 1,
            child: Column(
              children: [
                SizedBox(
                  height: 300,
                  child: InputWidget(
                    margin: const EdgeInsets.symmetric(vertical: 50),
                    padding: const EdgeInsets.symmetric(
                        horizontal: 20, vertical: 20),
                    hintText: 'Nhập khóa alphabet của bạn vào đây',
                    title: 'Khóa',
                    maxLines: 2,
                    controller: widget.keyController,
                    validator: (value) {
                      if (value?.isNotEmpty == true) {                                                
                        if ((value! + value.toUpperCase()).length != 52 || Utils.findDuplicate(value)) {
                          return 'Vui lòng kiểm tra lại độ dài khóa và kí tự khóa không được trùng nhau';
                        } else {
                          return null;
                        }
                      } else {
                        return 'Vui lòng nhập khóa';
                      }
                    },
                  ),
                ),
                ButtonWidget(
                  title: 'Bắt đầu mã hóa',
                  onTap: () {
                    if (_formKey.currentState!.validate()) {
                      widget.onTap();
                    }
                  },
                ),
              ],
            ),
          ),
          Flexible(
            flex: 1,
            child: InputWidget(
              hintText: 'Bản mã sẽ xuất hiện ở đây',
              // enable: false,
              readOnly: true,
              title: 'Bản mã',
              controller: _cipherTextController,
            ),
          ),
        ],
      ),
    );
  }
}
