import 'package:flutter/material.dart';

import '../../utils/utils.dart';
import '../../widgets/button_widget.dart';
import '../../widgets/input_widget.dart';

class DecodePermutation extends StatefulWidget {
  final TextEditingController plainTextController;
  final TextEditingController keyController;
  final TextEditingController cipherTextController;
  final Function() onTap;

  const DecodePermutation(
      {Key? key,
      required this.plainTextController,
      required this.keyController,
      required this.cipherTextController,
      required this.onTap})
      : super(key: key);

  @override
  State<DecodePermutation> createState() => _DecodePermutationState();
}

class _DecodePermutationState extends State<DecodePermutation> {
  late TextEditingController _cipherTextController;
  @override
  void initState() {
    _cipherTextController = widget.cipherTextController;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Flexible(
          flex: 1,
          child: InputWidget(
            hintText: 'Nhập bản mã của bạn vào đây',
            title: 'Bản mã',
            controller: widget.plainTextController,
          ),
        ),
        Flexible(
          flex: 1,
          child: Column(
            children: [
              SizedBox(
                height: 300,
                child: InputWidget(
                  margin: const EdgeInsets.symmetric(vertical: 50),
                  padding:
                      const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
                  hintText: 'Nhập khóa alphabet của bạn vào đây',
                  title: 'Khóa',
                  maxLines: 2,
                  controller: widget.keyController,
                  validator: (value) {
                    if (value?.isNotEmpty == true) {
                      if ((value! + value.toUpperCase()).length != 52 ||
                          Utils.findDuplicate(value)) {
                        return 'Vui lòng kiểm tra lại độ dài khóa và kí tự khóa không được trùng nhau';
                      } else {
                        return null;
                      }
                    } else {
                      return 'Vui lòng nhập khóa';
                    }
                  },
                ),
              ),
              ButtonWidget(
                title: 'Bắt đầu giải mã',
                onTap: widget.onTap,
              ),
            ],
          ),
        ),
        Flexible(
          flex: 1,
          child: InputWidget(
            hintText: 'Bản rõ sẽ xuất hiện ở đây',
            // enable: false,
            readOnly: true,
            title: 'Bản rõ',
            controller: _cipherTextController,
          ),
        ),
      ],
    );
  }
}
