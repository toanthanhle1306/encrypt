import 'package:get/get.dart';
import 'package:get/instance_manager.dart';
import 'permutation_controller.dart';

class PermutationBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => PermutationController());
  }
}
