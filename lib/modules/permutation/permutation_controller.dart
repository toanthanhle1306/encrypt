import 'package:flutter/material.dart';
import 'package:get/get.dart';

class PermutationController extends GetxController
    with GetSingleTickerProviderStateMixin {
  late TabController tabController;

  final inputTextController = TextEditingController();
  final keyController = TextEditingController();
  final outputTextController = TextEditingController();

  @override
  void onInit() async {
    super.onInit();
    tabController = TabController(vsync: this, length: 2);
    tabController.addListener(() {
      if (tabController.indexIsChanging) {
        inputTextController.clear();
        keyController.clear();
        outputTextController.clear();
      }
    });
  }

  void processPermutation({
    required String text,
    required String oldAlphabet,
    required String newAlphabet,
  }) {
    String result = '';
    int lengthText = text.length;

    for (int i = 0; i < lengthText; i++) {
      int oldCharIndex = oldAlphabet.indexOf(text[i]);

      if (oldCharIndex >= 0) {
        result += newAlphabet[oldCharIndex];
      } else {
        result += text[i];
      }
    }
    outputTextController.text = result;
  }
}
