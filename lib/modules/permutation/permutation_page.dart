import 'package:encrypt/common/theme/app_text_theme.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../utils/utils.dart';
import 'permutation_controller.dart';
import 'components/decode_permutation.dart';
import 'components/encode_permutation.dart';

class PermutationPage extends GetWidget<PermutationController> {
  const PermutationPage({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
          appBar: AppBar(
            title: const Text('Mã hóa Permutation'),
          ),
          backgroundColor: context.theme.backgroundColor,
          extendBody: true,
          body: PageView(
            physics: const NeverScrollableScrollPhysics(),
            children: [
              SizedBox(
                height: 500,
                child: DefaultTabController(
                    length: 2,
                    child: Scaffold(
                      appBar: PreferredSize(
                        preferredSize: const Size.fromHeight(kToolbarHeight),
                        child: Stack(
                          children: [
                            TabBar(
                              indicatorPadding:
                                  const EdgeInsets.symmetric(horizontal: 50),
                              controller: controller.tabController,
                              labelPadding:
                                  const EdgeInsets.symmetric(vertical: 10),
                              labelStyle:
                                  h18.copyWith(fontWeight: FontWeight.bold),
                              tabs: const [
                                Tab(
                                  text: "Mã hóa",
                                ),
                                Tab(
                                  text: "Giải mã",
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                      body: TabBarView(
                        controller: controller.tabController,
                        children: [
                          EncodePermutation(
                            plainTextController: controller.inputTextController,
                            cipherTextController:
                                controller.outputTextController,
                            keyController: controller.keyController,
                            onTap: () {
                              //hen Toi thu Bay
                              controller.processPermutation(
                                  text: controller.inputTextController.text,
                                  oldAlphabet:
                                      'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',
                                  newAlphabet: controller.keyController.text +
                                      controller.keyController.text
                                          .toUpperCase());
                            },
                          ),
                          // xnyahpogzqwbtsflrcvmuekjdi
                          DecodePermutation(
                            plainTextController: controller.inputTextController,
                            cipherTextController:
                                controller.outputTextController,
                            keyController: controller.keyController,
                            onTap: () {
                              //ghs Mfz mgu Nxd
                              controller.processPermutation(
                                  text: controller.inputTextController.text,
                                  oldAlphabet: controller.keyController.text +
                                      controller.keyController.text
                                          .toUpperCase(),
                                  newAlphabet:
                                      'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ');
                            },
                          ),
                        ],
                      ),
                    )),
              ),
            ],
          )),
    );
  }
}
