import 'package:encrypt/modules/utils/utils.dart';
import 'package:encrypt/modules/widgets/grid_input_widget.dart';
import 'package:flutter/material.dart';

import '../../widgets/button_widget.dart';
import '../../widgets/input_widget.dart';

class EncodeHill extends StatefulWidget {
  final TextEditingController plainTextController;
  final int size;
  final List<TextEditingController> keyListController;

  final TextEditingController cipherTextController;
  final Function onTap;

  const EncodeHill(
      {Key? key,
      required this.plainTextController,
      required this.size,
      required this.keyListController,
      required this.cipherTextController,
      required this.onTap})
      : super(key: key);

  @override
  State<EncodeHill> createState() => _EncodeHillState();
}

class _EncodeHillState extends State<EncodeHill> {
  late TextEditingController _cipherTextController;
  final _formKey = GlobalKey<FormState>();
  @override
  void initState() {
    _cipherTextController = widget.cipherTextController;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Flexible(
            flex: 1,
            child: InputWidget(
              hintText: 'Nhập bản rõ của bạn vào đây',
              title: 'Bản rõ',
              controller: widget.plainTextController,
              validator: (value) {
                return value?.isNotEmpty == true
                    ? null
                    : 'Vui lòng nhập bản rõ';
              },
            ),
          ),
          Flexible(
            flex: 1,
            child: Column(
              children: [
                Container(
                  margin: const EdgeInsets.only(top: 50.0),
                  child: const Text(
                    "Nhập khóa Hill ma trận 2x2",
                    style: TextStyle(
                      fontSize: 24,
                      fontWeight: FontWeight.bold,
                      color: Colors.deepOrangeAccent,
                    ),
                  ),
                ),
                Container(
                  padding: const EdgeInsets.all(0),
                  height: 300,
                  width: 400,
                  child: SizedBox.expand(
                    child: GridInputWidget(
                      keyboardType: TextInputType.number,
                      // margin: const EdgeInsets.symmetric(vertical: 30),
                      // padding: const EdgeInsets.symmetric(
                      //     horizontal: 20, vertical: 20),
                      hintText: 'Nhập số',
                      size: 2,
                      title: '',
                      controller: widget.keyListController,
                      validator: (value) {
                        if (value?.isNotEmpty == true) {
                          if (Utils().isNumeric(value!)) {
                            return null;
                          } else {
                            return 'Key phải là số';
                          }
                        } else {
                          return 'Vui lòng nhập khóa';
                        }
                      },
                    ),
                  ),
                ),
                ButtonWidget(
                  title: 'Bắt đầu mã hóa',
                  onTap: () {
                    if (_formKey.currentState!.validate()) {
                      widget.onTap();
                    }
                  },
                ),
              ],
            ),
          ),
          Flexible(
            flex: 1,
            child: InputWidget(
              hintText: 'Bản mã sẽ xuất hiện ở đây',
              // enable: false,
              readOnly: true,
              title: 'Bản mã',
              controller: _cipherTextController,
            ),
          ),
        ],
      ),
    );
  }
}
