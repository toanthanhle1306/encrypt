import 'dart:io';

import 'package:encrypt/modules/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';

class HillController extends GetxController
    with GetSingleTickerProviderStateMixin {
  late TabController tabController;
  final int size = 2;
  final inputTextController = TextEditingController();
  final outputTextController = TextEditingController();
  late List<TextEditingController> listKeyController;
  var keyNum = [].obs;

  @override
  void onInit() async {
    tabController = TabController(vsync: this, length: 2);
    listKeyController = [
      for (var i = 0; i < size * size; i++) TextEditingController()
    ];
    tabController.addListener(() {
      if (tabController.indexIsChanging) {
        inputTextController.clear();
        for (var i = 0; i < listKeyController.length; i++) {
          listKeyController.elementAt(i).clear();
        }
        outputTextController.clear();
      }
    });
    super.onInit();
  }

  int mod26(int x) {
    return x > 0 ? (x % 26) : 26 - (x.abs() % 26);
  }

  // Det = a*d - b*c
  int _findDet(int a, int b, int c, int d) {
    return mod26(a * d - b * c);
  }

  int _findDetInverse(int det) {
    return Utils().nghichDaoZn(det, 26);
  }

  void processVigenere(
      {required String text,
      required List<TextEditingController> key,
      required bool isEncrypt}) {
    String _text = text;
    List<int> _keyArray = [];
    String _result = "";
    int det = 1;
    int detNghichDao = 1;

    try {
      // Tạo các số trong ma trận
      if (size == 2) {
        for (var i = 0; i < key.length; i++) {
          _keyArray.add(int.parse(key.elementAt(i).text));
        }
      }
      print(_keyArray);
      det = _findDet(_keyArray[0], _keyArray[1], _keyArray[2], _keyArray[3]);
      detNghichDao = _findDetInverse(det);

      print("detNghichDao: $detNghichDao");

      if (detNghichDao == -1) {
        EasyLoading.showError('Ma trận key không hợp lệ!');
        return;
      }
    } catch (e) {
      EasyLoading.showError('Key phải là chuỗi kí tự trong bảng chữ cái!');
    }

    // int spaceCount = 0;
    var textNoSpace = _text.replaceAll(RegExp(r' '), '');
    if (textNoSpace.length % 2 == 1) {
      textNoSpace += 'x';
    } // xóa khoảng trắng và làm chẵn chuỗi
    for (int i = 0; i < textNoSpace.length; i = i + size) {
      // lấy vị trí của kí tự trong bảng mã ASCII
      int ch1 = textNoSpace.toUpperCase().codeUnitAt(i);
      int ch2 = textNoSpace.toUpperCase().codeUnitAt(i + 1);
      int offset;
      String h;
      if (ch1 >= 'a'.codeUnitAt(0) && ch1 <= 'z'.codeUnitAt(0)) {
        offset = 97;
      } else if (ch1 >= 'A'.codeUnitAt(0) && ch1 <= 'Z'.codeUnitAt(0)) {
        offset = 65;
      }
      // Already removed space before
      // else if (ch1 == ' '.codeUnitAt(0)) {
      //   _result += " ";
      //   // spaceCount++;
      //   continue;
      // }
      else {
        EasyLoading.showError('Dữ liệu không hợp lệ');
        _result = "";
        break;
      }

      int charOne, charTwo;
      int newCharOne, newCharTwo;
      if (isEncrypt) {
        // Cột đầu tiên: xài key[0] và [2], Cột thứ hai: xài key[1] và [3]
        charOne = mod26(ch1 - offset);
        charTwo = mod26(ch2 - offset);
        // print("($charOne, $charTwo)");
        newCharOne = mod26(charOne * _keyArray[0] + charTwo * _keyArray[2]);
        newCharTwo = mod26(charOne * _keyArray[1] + charTwo * _keyArray[3]);
      } else {
        
        charOne = mod26(ch1 - offset);
        charTwo = mod26(ch2 - offset);

        //[3, 3, 2, 5]  => [5, -3, -2, 3]
        var matrixInverse = [
          mod26(_keyArray[3] * detNghichDao),
          mod26(((-1) * _keyArray[1]) * detNghichDao),
          mod26(((-1) * _keyArray[2]) * detNghichDao),
          mod26(_keyArray[0] * detNghichDao)
        ];  

        newCharOne =
            mod26(charOne * matrixInverse[0] + charTwo * matrixInverse[2]);
        newCharTwo = mod26(charOne * matrixInverse[1] + charTwo * matrixInverse[3]);
      }
      h = String.fromCharCode(newCharOne + offset) +
          String.fromCharCode(newCharTwo + offset);
      _result += h;
    }

    outputTextController.text = _result;
  }
}
