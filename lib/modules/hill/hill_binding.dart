import 'package:get/get.dart';
import 'package:get/instance_manager.dart';
import 'hill_controller.dart';

class HillBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => HillController());
  }
}
