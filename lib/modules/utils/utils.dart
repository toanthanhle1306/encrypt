class Utils {
  int nghichDaoZn(int a, int n) {
    int originNum = n; // xử lí trường hợp nghịch đảo tính ra mod âm
    int x = 0, y = 0;
    int x1 = 0, x2 = 1, y1 = 1, y2 = 0;
    while (n > 0) {
      int r = a % n;
      int q = a ~/ n;
      x = x2 - q * x1;
      y = y2 - q * y1;
      a = n;
      n = r;
      x2 = x1;
      x1 = x;
      y2 = y1;
      y1 = y;
    }
    if (a > 1) {
      // không có phần tử nghịch đảo
      return -1;
    }

    return x2 > 0
        ? x2
        : x2 +
            originNum; // trường hợp phần tử nghịch đảo âm thì phải cộng thêm vành
  }

  bool isNumeric(String string) {
    // Null or empty string is not a number
    if (string.isEmpty) {
      return false;
    }

    // Try to parse input string to number.
    // Both integer and double work.
    // Use int.tryParse if you want to check integer only.
    // Use double.tryParse if you want to check double only.
    final number = num.tryParse(string);

    if (number == null) {
      return false;
    }

    return true;
  }

  static bool findDuplicate(String str) {
    bool hasDuplicated = false;
    var arrayChar = str.split("");
    var myMap = Map<String, int>.fromIterables(
        arrayChar, List.generate(arrayChar.length, (i) => 0));
    for (var e in arrayChar) {
      myMap[e] = myMap[e]! + 1;
    }
    print(myMap);
    myMap.forEach((key, value) {
      if (value > 1) {
        hasDuplicated = true;
      }
      return;
    });
    return hasDuplicated;
  }
}
