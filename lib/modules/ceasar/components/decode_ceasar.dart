import 'package:flutter/material.dart';

import '../../widgets/button_widget.dart';
import '../../widgets/input_widget.dart';

class DecodeCeasar extends StatefulWidget {
  final TextEditingController plainTextController;
  final TextEditingController keyController;
  final TextEditingController cipherTextController;
  final Function() onTap;

  const DecodeCeasar(
      {Key? key,
      required this.plainTextController,
      required this.keyController,
      required this.cipherTextController,
      required this.onTap})
      : super(key: key);

  @override
  State<DecodeCeasar> createState() => _DecodeCeasarState();
}

class _DecodeCeasarState extends State<DecodeCeasar> {
  late TextEditingController _cipherTextController;
  @override
  void initState() {
    _cipherTextController = widget.cipherTextController;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Flexible(
          flex: 1,
          child: InputWidget(
            hintText: 'Nhập bản mã của bạn vào đây',
            title: 'Bản mã',
            controller: widget.plainTextController,
          ),
        ),
        Flexible(
          flex: 1,
          child: Column(
            children: [
              SizedBox(
                height: 300,
                child: InputWidget(
                  keyboardType: TextInputType.number,
                  margin: const EdgeInsets.symmetric(vertical: 50),
                  padding:
                      const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
                  hintText: 'Nhập khóa của bạn vào đây',
                  title: 'Khóa',
                  maxLines: 2,
                  controller: widget.keyController,
                ),
              ),
              ButtonWidget(
                title: 'Bắt đầu giải mã',
                onTap: widget.onTap,
              ),
            ],
          ),
        ),
        Flexible(
          flex: 1,
          child: InputWidget(
            hintText: 'Bản rõ sẽ xuất hiện ở đây',
            // enable: false,
            readOnly: true,
            title: 'Bản rõ',
            controller: _cipherTextController,
          ),
        ),
      ],
    );
  }
}
