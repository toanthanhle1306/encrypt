import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';

class CeasarController extends GetxController
    with GetSingleTickerProviderStateMixin {
  late TabController tabController;

  final inputTextController = TextEditingController();
  final keyController = TextEditingController();
  final outputTextController = TextEditingController();

  @override
  void onInit() async {
    super.onInit();
    tabController = TabController(vsync: this, length: 2);
    tabController.addListener(() {
      if (tabController.indexIsChanging) {
        inputTextController.clear();
        keyController.clear();
        outputTextController.clear();
      }
    });
  }

  void processCeasar(
      {required String text, required String key, required bool isEncrypt}) {
    String _text = text;
    late int _key;
    String _result = "";

    try {
      _key = int.parse(key);
    } catch (e) {
      EasyLoading.showError('Key phải là số');
    }

    for (int i = 0; i < _text.length; i++) {
      int ch = _text.codeUnitAt(i); //lấy vị trí của kí tự trong bảng mã ASCII
      int offset;
      String h;
      if (ch >= 'a'.codeUnitAt(0) && ch <= 'z'.codeUnitAt(0)) {
        offset = 97;
      } else if (ch >= 'A'.codeUnitAt(0) && ch <= 'Z'.codeUnitAt(0)) {
        offset = 65;
      } else if (ch == ' '.codeUnitAt(0)) {
        _result += " ";
        continue;
      } else {
        EasyLoading.showError('Dữ liệu không hợp lệ');
        _result = "";
        break;
      }

      int c;
      if (isEncrypt) {
        c = (ch + _key - offset) % 26;
      } else {
        c = (ch - _key - offset) % 26;
      }
      h = String.fromCharCode(c + offset);
      _result += h;
    }

    outputTextController.text = _result;
  }
}
