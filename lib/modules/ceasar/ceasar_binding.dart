import 'package:get/get.dart';
import 'package:get/instance_manager.dart';
import 'ceasar_controller.dart';

class CeasarBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => CeasarController());
  }
}
