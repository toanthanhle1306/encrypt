import 'package:encrypt/data/model/classical_cipher.dart';
import 'package:encrypt/routes/app_pages.dart';
import 'package:get/get.dart';

class HomeController extends GetxController {
  final Rx<List> isHovering =
      Rx<List>([false, false, false, false, false, false, false]);

  void routes(ClassicalCipherType type) {
    switch (type) {
      case ClassicalCipherType.Ceasar:
        Get.toNamed(Routes.CEASAR);
        break;
      case ClassicalCipherType.Permutation:
        Get.toNamed(Routes.PERMUTATION);
        break;
      case ClassicalCipherType.Affine:
        Get.toNamed(Routes.AFFINE);
        break;
      case ClassicalCipherType.Vigenere:
        Get.toNamed(Routes.VIGENERE);
        break;
      case ClassicalCipherType.Hill:
        Get.toNamed(Routes.HILL);
        break;
      case ClassicalCipherType.RSA:
        Get.toNamed(Routes.RSA);
        break;
      case ClassicalCipherType.Elgamal:
        Get.toNamed(Routes.ELGAMAL);
        break;
      default:
    }
  }
}
