import 'package:encrypt/data/model/classical_cipher.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../common/theme/themes.dart';
import 'home_controller.dart';

class HomePage extends GetWidget<HomeController> {
  const HomePage({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
          appBar: AppBar(
            title: const Text('Chương trình mã hóa cổ điển'),
          ),
          backgroundColor: context.theme.backgroundColor,
          extendBody: true,
          body: Column(
            children: [
              Expanded(
                child: Center(
                  child: SizedBox(
                    width: 500,
                    child: ListView.separated(
                        shrinkWrap: true,
                        padding: const EdgeInsets.all(20),
                        itemBuilder: (context, index) {
                          final item = ClassicalCipher.cipherList[index];
                          return Obx(() {
                            return InkWell(
                              onHover: (value) {
                                value
                                    ? controller.isHovering.value[index] = true
                                    : controller.isHovering.value[index] =
                                        false;
                                controller.isHovering.refresh();
                              },
                              onTap: () {
                                controller.routes(item.type);
                              },
                              child: Container(
                                padding: const EdgeInsets.all(20),
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(10),
                                    color: controller.isHovering.value[index]
                                        ? orangeClr.withOpacity(0.9)
                                        : orangeClr),
                                child: Row(
                                  mainAxisSize: MainAxisSize.min,
                                  children: [
                                    Expanded(
                                      child: Text(
                                        item.title,
                                        style: TextStyle(
                                            color: controller
                                                    .isHovering.value[index]
                                                ? yellowClr
                                                : Colors.white),
                                      ),
                                    ),
                                    const Icon(Icons.arrow_right_alt_rounded)
                                  ],
                                ),
                              ),
                            );
                          });
                        },
                        separatorBuilder: (context, index) => const SizedBox(
                              height: 20,
                            ),
                        itemCount: ClassicalCipher.cipherList.length),
                  ),
                ),
              ),
            ],
          )),
    );
  }
}
