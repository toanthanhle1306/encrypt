import 'package:get/get.dart';
import 'package:get/instance_manager.dart';
import 'affine_controller.dart';

class AffineBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => AffineController());
  }
}
