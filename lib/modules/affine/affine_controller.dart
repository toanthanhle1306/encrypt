import 'package:encrypt/modules/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';

class AffineController extends GetxController
    with GetSingleTickerProviderStateMixin {
  late TabController tabController;

  final inputTextController = TextEditingController();
  final keyAController = TextEditingController();
  final keyBController = TextEditingController();
  final outputTextController = TextEditingController();

  @override
  void onInit() async {
    super.onInit();
    tabController = TabController(vsync: this, length: 2);
    tabController.addListener(() {
      if (tabController.indexIsChanging) {
        inputTextController.clear();
        keyAController.clear();
        keyBController.clear();
        outputTextController.clear();
      }
    });
  }

  void processAffine(
      {required String text,
      required String keyA,
      required String keyB,
      required bool isEncrypt}) {
    String _text = text;
    late int _keyA;
    late int _keyB;
    String _result = "";

    try {
      _keyA = int.parse(keyA);
      _keyB = int.parse(keyB);
    } catch (e) {
      EasyLoading.showError('Key phải là số');
    }

    for (int i = 0; i < _text.length; i++) {
      int ch = _text.codeUnitAt(i); //lấy vị trí của kí tự trong bảng mã ASCII
      int offset;
      String h;
      if (ch >= 'a'.codeUnitAt(0) && ch <= 'z'.codeUnitAt(0)) {
        offset = 97;
      } else if (ch >= 'A'.codeUnitAt(0) && ch <= 'Z'.codeUnitAt(0)) {
        offset = 65;
      } else if (ch == ' '.codeUnitAt(0)) {
        _result += " ";
        continue;
      } else {
        EasyLoading.showError('Dữ liệu không hợp lệ');
        _result = "";
        break;
      }

      int c;
      if (isEncrypt) {
        c = (_keyA * (ch - offset) + _keyB) % 26;
      } else {
        int nghichDao = Utils().nghichDaoZn(_keyA, 26);
        // print(nghichDao);
        c = (nghichDao * (ch - offset - _keyB)) % 26;
      }
      h = String.fromCharCode(c + offset);
      _result += h;
    }

    outputTextController.text = _result;
  }
}
