import 'package:encrypt/modules/utils/utils.dart';
import 'package:flutter/material.dart';

import '../../widgets/button_widget.dart';
import '../../widgets/input_widget.dart';

class DecodeAffine extends StatefulWidget {
  final TextEditingController plainTextController;
  final TextEditingController keyAController;
  final TextEditingController keyBController;
  final TextEditingController cipherTextController;
  final Function() onTap;

  const DecodeAffine(
      {Key? key,
      required this.plainTextController,
      required this.keyAController,
      required this.keyBController,
      required this.cipherTextController,
      required this.onTap})
      : super(key: key);

  @override
  State<DecodeAffine> createState() => _DecodeAffineState();
}

class _DecodeAffineState extends State<DecodeAffine> {
  late TextEditingController _cipherTextController;
  @override
  void initState() {
    _cipherTextController = widget.cipherTextController;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Flexible(
          flex: 1,
          child: InputWidget(
            hintText: 'Nhập bản mã của bạn vào đây',
            title: 'Bản mã',
            controller: widget.plainTextController,
          ),
        ),
        Flexible(
          flex: 1,
          child: Column(
            children: [
              SizedBox(
                height: 300,
                child: InputWidget(
                  keyboardType: TextInputType.number,
                  margin: const EdgeInsets.symmetric(vertical: 50),
                  padding:
                      const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
                  hintText: 'Nhập khóa A của bạn vào đây',
                  title: 'Khóa A',
                  maxLines: 2,
                  controller: widget.keyAController,
                  validator: (value) {
                    if (value?.isNotEmpty == true) {
                      if (Utils().isNumeric(value!) &&
                          Utils().nghichDaoZn(int.parse(value), 26) != -1) {
                        return null;
                      } else {
                        return 'Key phải là số nghịch đảo trong bảng mã 26: {1,3,5,7,9,11,15,17,19,21,23,25}';
                      }
                    } else {
                      return 'Vui lòng nhập khóa';
                    }
                  },
                ),
              ),
              SizedBox(
                height: 300,
                child: InputWidget(
                  keyboardType: TextInputType.number,
                  margin: const EdgeInsets.symmetric(vertical: 50),
                  padding:
                      const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
                  hintText: 'Nhập khóa B của bạn vào đây',
                  title: 'Khóa B',
                  maxLines: 2,
                  controller: widget.keyBController,
                  validator: (value) {
                    if (value?.isNotEmpty == true) {
                      if (Utils().isNumeric(value!)) {
                        return null;
                      } else {
                        return 'Key phải là số';
                      }
                    } else {
                      return 'Vui lòng nhập khóa';
                    }
                  },
                ),
              ),
              ButtonWidget(
                title: 'Bắt đầu giải mã',
                onTap: widget.onTap,
              ),
            ],
          ),
        ),
        Flexible(
          flex: 1,
          child: InputWidget(
            hintText: 'Bản rõ sẽ xuất hiện ở đây',
            // enable: false,
            readOnly: true,
            title: 'Bản rõ',
            controller: _cipherTextController,
          ),
        ),
      ],
    );
  }
}
