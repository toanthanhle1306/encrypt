import 'package:flutter/material.dart';

import '../../common/theme/app_text_theme.dart';
import '../../common/theme/themes.dart';

class GridInputWidget extends StatelessWidget {
  final String title;
  final String hintText;
  final List<TextEditingController> controller;
  final int maxLines;
  final int minLines;
  final int? size;
  final bool? readOnly;
  final String? Function(String?)? validator;
  final Function(String)? onChanged;
  final bool? enable;
  final EdgeInsetsGeometry? margin;
  final EdgeInsetsGeometry? padding;
  final TextInputType keyboardType;
  const GridInputWidget(
      {Key? key,
      required this.title,
      required this.hintText,
      required this.controller,
      this.maxLines = 1,
      this.minLines = 1,
      this.size = 2,
      this.readOnly,
      this.validator,
      this.onChanged,
      this.enable,
      this.margin = const EdgeInsets.all(50),
      this.padding = const EdgeInsets.all(20),
      this.keyboardType = TextInputType.text})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: padding,
      // color: Colors.red,
      child: GridView.builder(
        padding: padding,
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          // maxCrossAxisExtent: 200,
          crossAxisCount: size!,
          childAspectRatio: 3 / 2,
          crossAxisSpacing: 20,
          mainAxisSpacing: 20,
        ),
        itemCount: (size! * size!),
        itemBuilder: (BuildContext ctx, index) {
          return Container(
            padding: padding,
            alignment: Alignment.center,
            color: Colors.amber,
            child: Center(
              child: TextFormField(
                enabled: enable,
                readOnly: readOnly ?? false,
                validator: validator,
                onChanged: onChanged,
                autofocus: false,
                controller: controller.elementAt(index),
                style: h18.copyWith(color: Colors.black),
                maxLines: maxLines,
                keyboardType: keyboardType,
                decoration: InputDecoration(
                    hintText: hintText,
                    hintStyle: h18.copyWith(color: Colors.grey),
                    focusedBorder: InputBorder.none,
                    enabledBorder: InputBorder.none),
              ),
            ),
          );
        },
      ),
    );
  }
}
