import 'package:flutter/material.dart';

import '../../common/theme/app_text_theme.dart';
import '../../common/theme/themes.dart';

class InputWidget extends StatelessWidget {
  final String title;
  final String hintText;
  final TextEditingController? controller;
  final int maxLines;
  final int minLines;
  final bool? readOnly;
  final String? Function(String?)? validator;
  final Function(String)? onChanged;
  final bool? enable;
  final EdgeInsetsGeometry? margin;
  final EdgeInsetsGeometry? padding;
  final TextInputType keyboardType;
  const InputWidget(
      {Key? key,
      required this.title,
      required this.hintText,
      this.controller,
      this.maxLines = 25,
      this.minLines = 10,
      this.readOnly,
      this.validator,
      this.onChanged,
      this.enable,
      this.margin = const EdgeInsets.all(50),
      this.padding = const EdgeInsets.all(20),
      this.keyboardType = TextInputType.text})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: padding,
      margin: margin,
      decoration: BoxDecoration(
          color: Colors.white, borderRadius: BorderRadius.circular(10)),
      child: Column(
        children: [
          Text(
            title,
            style: h18.copyWith(fontWeight: FontWeight.bold, color: yellowClr),
          ),
          const Padding(
            padding: EdgeInsets.symmetric(vertical: 8.0),
            child: Divider(
              color: Colors.black,
              height: 1,
            ),
          ),
          Expanded(
            child: Center(
              child: TextFormField(
                key: key,
                enabled: enable,
                readOnly: readOnly ?? false,
                validator: validator,
                onChanged: onChanged,
                autofocus: false,
                controller: controller,
                style: h18.copyWith(color: Colors.black),
                maxLines: maxLines,
                keyboardType: keyboardType,
                decoration: InputDecoration(
                    hintText: hintText,
                    hintStyle: h18.copyWith(color: Colors.grey),
                    focusedBorder: InputBorder.none,
                    enabledBorder: InputBorder.none),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
