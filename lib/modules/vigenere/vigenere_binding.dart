import 'package:get/get.dart';
import 'package:get/instance_manager.dart';
import 'vigenere_controller.dart';

class VigenereBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => VigenereController());
  }
}
