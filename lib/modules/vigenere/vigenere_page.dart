import 'package:encrypt/common/theme/app_text_theme.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'vigenere_controller.dart';
import 'components/decode_vigenere.dart';
import 'components/encode_vigenere.dart';

class VigenerePage extends GetWidget<VigenereController> {
  const VigenerePage({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
          appBar: AppBar(
            title: const Text('Mã hóa Vigenere'),
          ),
          backgroundColor: context.theme.backgroundColor,
          extendBody: true,
          body: PageView(
            physics: const NeverScrollableScrollPhysics(),
            children: [
              SizedBox(
                height: 500,
                child: DefaultTabController(
                    length: 2,
                    child: Scaffold(
                      appBar: PreferredSize(
                        preferredSize: const Size.fromHeight(kToolbarHeight),
                        child: Stack(
                          children: [
                            TabBar(
                              indicatorPadding:
                                  const EdgeInsets.symmetric(horizontal: 50),
                              controller: controller.tabController,
                              labelPadding:
                                  const EdgeInsets.symmetric(vertical: 10),
                              labelStyle:
                                  h18.copyWith(fontWeight: FontWeight.bold),
                              tabs: const [
                                Tab(
                                  text: "Mã hóa",
                                ),
                                Tab(
                                  text: "Giải mã",
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                      body: TabBarView(
                        controller: controller.tabController,
                        children: [
                          EncodeVigenere(
                            plainTextController: controller.inputTextController,
                            cipherTextController:
                                controller.outputTextController,
                            keyController: controller.keyController,
                            onTap: () {
                              controller.processVigenere(
                                  text: controller.inputTextController.text,
                                  key: controller.keyController.text,
                                  isEncrypt: true);
                            },
                          ),
                          DecodeVigenere(
                            plainTextController: controller.inputTextController,
                            cipherTextController:
                                controller.outputTextController,
                            keyController: controller.keyController,
                            onTap: () {
                              controller.processVigenere(
                                  text: controller.inputTextController.text,
                                  key: controller.keyController.text,
                                  isEncrypt: false);
                            },
                          ),
                        ],
                      ),
                    )),
              ),
            ],
          )),
    );
  }
}
