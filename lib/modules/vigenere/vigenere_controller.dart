import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';

class VigenereController extends GetxController
    with GetSingleTickerProviderStateMixin {
  late TabController tabController;

  final inputTextController = TextEditingController();
  final keyController = TextEditingController();
  final outputTextController = TextEditingController();

  @override
  void onInit() async {
    super.onInit();
    tabController = TabController(vsync: this, length: 2);
    tabController.addListener(() {
      if (tabController.indexIsChanging) {
        inputTextController.clear();
        keyController.clear();
        outputTextController.clear();
      }
    });
  }

  void processVigenere(
      {required String text, required String key, required bool isEncrypt}) {
    String _text = text;
    var _keyArray = [];
    String _result = "";

    try {
      // Tạo key từ chuỗi
      String lowerCaseKey = key.toLowerCase();
      if (!RegExp(r'^[a-zA-Z]+$').hasMatch(lowerCaseKey)) {
        EasyLoading.showError('Key phải là chuỗi kí tự trong bảng chữ cái!');
        return;
      }
      for (var i = 0; i < lowerCaseKey.length; i++) {
        int ch = lowerCaseKey
            .codeUnitAt(i); // lấy vị trí của kí tự trong bảng mã ASCII
        _keyArray.add((ch - 97).toString());
      }
      print(_keyArray);
    } catch (e) {
      EasyLoading.showError('Key phải là chuỗi kí tự trong bảng chữ cái!');
    }

    int spaceCount = 0;
    for (int i = 0; i < _text.length; i++) {
      int ch = _text.codeUnitAt(i); //lấy vị trí của kí tự trong bảng mã ASCII
      int offset;
      String h;
      if (ch >= 'a'.codeUnitAt(0) && ch <= 'z'.codeUnitAt(0)) {
        offset = 97;
      } else if (ch >= 'A'.codeUnitAt(0) && ch <= 'Z'.codeUnitAt(0)) {
        offset = 65;
      } else if (ch == ' '.codeUnitAt(0)) {
        _result += " ";
        spaceCount++;
        continue;
      } else {
        EasyLoading.showError('Dữ liệu không hợp lệ');
        _result = "";
        break;
      }

      int c;
      int keyCharIndex = ((i - spaceCount) % _keyArray.length);
      int keyChar = int.parse(_keyArray[keyCharIndex]);
      if (isEncrypt) {
        c = (ch + keyChar - offset) % 26;
      } else {
        c = (ch - keyChar - offset) % 26;
      }
      h = String.fromCharCode(c + offset);
      _result += h;
    }

    outputTextController.text = _result;
  }
}
