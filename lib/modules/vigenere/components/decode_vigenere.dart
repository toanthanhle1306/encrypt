import 'package:flutter/material.dart';

import '../../widgets/button_widget.dart';
import '../../widgets/input_widget.dart';

class DecodeVigenere extends StatefulWidget {
  final TextEditingController plainTextController;
  final TextEditingController keyController;
  final TextEditingController cipherTextController;
  final Function() onTap;

  const DecodeVigenere(
      {Key? key,
      required this.plainTextController,
      required this.keyController,
      required this.cipherTextController,
      required this.onTap})
      : super(key: key);

  @override
  State<DecodeVigenere> createState() => _DecodeVigenereState();
}

class _DecodeVigenereState extends State<DecodeVigenere> {
  late TextEditingController _cipherTextController;
  @override
  void initState() {
    _cipherTextController = widget.cipherTextController;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Flexible(
          flex: 1,
          child: InputWidget(
            hintText: 'Nhập bản mã của bạn vào đây',
            title: 'Bản mã',
            controller: widget.plainTextController,
            validator: (value) {
              return value?.isNotEmpty == true ? null : 'Vui lòng nhập bản mã';
            },
          ),
        ),
        Flexible(
          flex: 1,
          child: Column(
            children: [
              SizedBox(
                height: 300,
                child: InputWidget(
                  // keyboardType: TextInputType.number,
                  margin: const EdgeInsets.symmetric(vertical: 50),
                  padding:
                      const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
                  hintText: 'Nhập khóa của bạn vào đây',
                  title: 'Khóa',
                  maxLines: 2,
                  controller: widget.keyController,
                  validator: (value) {
                    if (value?.isNotEmpty == true) {
                      if (RegExp(r'^[a-zA-Z]+$').hasMatch(value!)) {
                        return null;
                      } else {
                        return 'Key phải là chuỗi kí tự trong bảng chữ cái';
                      }
                    } else {
                      return 'Vui lòng nhập khóa';
                    }
                  },
                ),
              ),
              ButtonWidget(
                title: 'Bắt đầu giải mã',
                onTap: widget.onTap,
              ),
            ],
          ),
        ),
        Flexible(
          flex: 1,
          child: InputWidget(
            hintText: 'Bản rõ sẽ xuất hiện ở đây',
            // enable: false,
            readOnly: true,
            title: 'Bản rõ',
            controller: _cipherTextController,
          ),
        ),
      ],
    );
  }
}
