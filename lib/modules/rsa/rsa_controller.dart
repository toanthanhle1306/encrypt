import 'dart:convert';
import 'dart:math';

import 'package:encrypt/modules/rsa/components/key_rsa_generator.dart';
import 'package:encrypt/modules/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';

class RSAController extends GetxController
    with GetSingleTickerProviderStateMixin {
  late TabController tabController;

  final inputTextController = TextEditingController();
  final keyController = TextEditingController();
  final outputTextController = TextEditingController();
  final Rx<KeyRSA> keyPair = KeyRSA().obs;

  @override
  void onInit() async {
    super.onInit();
    tabController = TabController(vsync: this, length: 2);
    tabController.addListener(() {
      if (tabController.indexIsChanging) {
        keyController.clear();
        outputTextController.clear();
      }
    });
  }

  void processRSA(
      {required String text, required String key, required bool isEncrypt}) {
    String _text = text;
    List<BigInt> ketQuaMaHoa = [];
    List<int> ketQuaGiaiMa = [];

    // Xem class key_generator trong thư mục components để xem phiN
    if (isEncrypt) {
      try {
        var split = key.split(",");
        if (split.length == 2) {
          // ----- Test: (n,e) = 1137554533, 781801.  d = 1137554533, 390151741
          // BigInt n = BigInt.from(1137554533);
          // BigInt e = BigInt.from(781801);

          BigInt n = BigInt.parse(split[0]);
          BigInt e = BigInt.parse(split[1]);

          var plainText = utf8.encode(text);
          print(plainText);
          plainText.forEach((charNum) {
            // modPow là 1 số mũ e rồi mod n
            ketQuaMaHoa.add(BigInt.from(charNum).modPow(e, n));
          });
          print(ketQuaMaHoa);

          // Giải mã thì lấy giá trị số mã hóa mũ d rồi mod n như dòng modPow(d, n)
          // BigInt d = BigInt.from(390151741);
          // ketQuaMaHoa.forEach((charNum) {
          //   ketQuaGiaiMa.add((charNum).modPow(d, n).toInt());
          // });
          // print(ketQuaGiaiMa);

          // var plainTextGiaiMa = utf8.decode(ketQuaGiaiMa); // từ số -> text
          // print(plainTextGiaiMa);
        } else {
          return;
        }
      } catch (e) {
        EasyLoading.showError('Key phải là chuỗi kí tự trong bảng chữ cái!');
      }
      bool isValidInt = true;
      ketQuaMaHoa.forEach((element) {
        if (!element.isValidInt) {
          isValidInt = false;
        }
      });
      outputTextController.text = ketQuaMaHoa.toString();
    } else {
      var cipherText = _text
          .replaceAll(RegExp(r'\['), "")
          .replaceAll(RegExp(r'\]'), "")
          .removeAllWhitespace
          .split(",");
      List<BigInt> listMaHoa = [];
      cipherText.forEach((giaTri) {
        listMaHoa.add(BigInt.parse(giaTri));
      });
      print(listMaHoa);

      var splitKey = key.split(",");
      if (splitKey.length == 2) {
        // ----- Test: (n,e) = 1137554533, 781801.  d = 1137554533, 390151741
        // BigInt n = BigInt.from(1137554533);
        // BigInt d = BigInt.from(390151741);

        BigInt n = BigInt.parse(splitKey[0]);
        BigInt d = BigInt.parse(splitKey[1]);

        // Giải mã thì lấy giá trị số mã hóa mũ d rồi mod n như dòng modPow(d, n)
        listMaHoa.forEach((charNum) {
          ketQuaGiaiMa.add((charNum).modPow(d, n).toInt());
        });
        print("ketQuaGiaiMa" + ketQuaGiaiMa.toString());

        var plainTextGiaiMa = utf8.decode(ketQuaGiaiMa); // từ số -> text
        print(plainTextGiaiMa);
        outputTextController.text = plainTextGiaiMa.toString();
      }
    }

    // ---------- biến mảng mã hóa thành chuỗi
    // if (isValidInt) {
    //   List<String> listMaHoaInt = [];
    //   for (var bigNumber in ketQuaMaHoa) {
    //     listMaHoaInt.add(bigNumber.toRadixString(16));
    //   }
    //   print(listMaHoaInt);
    //   String result = "";
    //   listMaHoaInt.forEach((n) {
    //     result += n;
    //     result += "#";
    //   });
    //   outputTextController.text = result;
    // }
  }
}
