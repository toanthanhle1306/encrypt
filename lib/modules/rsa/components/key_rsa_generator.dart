import 'dart:math';

class KeyRSA {
  late String _publicKey;
  late String _privateKey;
  String get publicKey => _publicKey;
  String get privateKey => _privateKey;

  KeyRSA() {
    _publicKey = "";
    _privateKey = "";
  }

  int _randomOddNumberRange(min, max) {
    int temp = min + Random.secure().nextInt(max - min);
    if (temp % 2 == 0) {
      temp -= 1;
    }
    if (temp % 10 == 0) {
      temp += 1;
    }
    if (temp % 5 == 0) {
      temp += 2;
    }
    return temp;
  }

  bool _checkIfPrime(int number) {
    if (number % 2 == 0) return false;
    var limit = sqrt(number).toInt();
    if (number % limit == 0) return false;

    for (var i = 3; i <= limit; i = i + 2) {
      if (number % i == 0) {
        return false;
      }
    }
    return true;
  }

  void generateKeysPair() {
    bool valid = false;
    int p = _randomOddNumberRange(20000, 99999);
    int q = _randomOddNumberRange(20000, 99999);
    while (!valid) {
      p = _randomOddNumberRange(20000, 99999);
      q = _randomOddNumberRange(20000, 99999);
      if (p != q) {
        if (_checkIfPrime(p) && _checkIfPrime(q)) {
          valid = true;
        } else {
          valid = false;
        }
      }
    }
    // Cặp số nguyên tố p và q
    // print("p=$p,  q=$q");
    String tempN = (p * q).toString();
    String tempPhiN = ((p - 1) * (q - 1)).toString();
    BigInt n = BigInt.parse(tempN);
    BigInt phiN = BigInt.parse(tempPhiN);
    print("n=$n,  phiN=$phiN");

    int e;
    BigInt d;
    do {
      e = _randomOddNumberRange(100, 999999);
    } while (!_checkIfPrime(e));
    d = (BigInt.from(e)).modInverse(phiN);

    // print("e=$e,  d=$d");
    _publicKey = "$n, $e";
    _privateKey = "$n, $d";
  }

  @override
  String toString() {
    return "{$publicKey,  $privateKey}";
  }
}
