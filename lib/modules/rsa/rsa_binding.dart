import 'package:get/get.dart';
import 'package:get/instance_manager.dart';
import 'rsa_controller.dart';

class RSABinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => RSAController());
  }
}
