import 'package:encrypt/common/theme/app_text_theme.dart';
import 'package:encrypt/modules/rsa/components/decode_rsa.dart';
import 'package:encrypt/modules/rsa/components/encode_rsa.dart';
import 'package:encrypt/modules/rsa/rsa_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class RSAPage extends GetWidget<RSAController> {
  const RSAPage({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
          appBar: AppBar(
            title: const Text('Mã hóa RSA'),
          ),
          backgroundColor: context.theme.backgroundColor,
          extendBody: true,
          body: PageView(
            physics: const NeverScrollableScrollPhysics(),
            children: [
              SizedBox(
                height: 500,
                child: DefaultTabController(
                    length: 2,
                    child: Scaffold(
                      appBar: PreferredSize(
                        preferredSize: const Size.fromHeight(kToolbarHeight),
                        child: Stack(
                          children: [
                            TabBar(
                              indicatorPadding:
                                  const EdgeInsets.symmetric(horizontal: 50),
                              controller: controller.tabController,
                              labelPadding:
                                  const EdgeInsets.symmetric(vertical: 10),
                              labelStyle:
                                  h18.copyWith(fontWeight: FontWeight.bold),
                              tabs: const [
                                Tab(
                                  text: "Mã hóa",
                                ),
                                Tab(
                                  text: "Giải mã",
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                      body: TabBarView(
                        controller: controller.tabController,
                        children: [
                          EncodeRSA(
                            plainTextController: controller.inputTextController,
                            cipherTextController:
                                controller.outputTextController,
                            keyController: controller.keyController,
                            encodeButton: () {
                              controller.processRSA(
                                  text: controller.inputTextController.text,
                                  key: controller.keyController.text,
                                  isEncrypt: true);
                            },
                          ),
                          DecodeRSA(
                            plainTextController: controller.inputTextController,
                            cipherTextController:
                                controller.outputTextController,
                            keyController: controller.keyController,
                            onTap: () {
                              controller.processRSA(
                                  text: controller.inputTextController.text,
                                  key: controller.keyController.text,
                                  isEncrypt: false);
                            },
                          ),
                        ],
                      ),
                    )),
              ),
            ],
          )),
    );
  }
}
